// GRADES

// Neo
const neoAbsoluteOration = 60;
const neoAbsoluteCrafts = 57;
const neoAbsolutePsychology = 72;
const neoAbsoluteChemistry = 88;


const neoOration = 0.5;
const neoCrafts = 0.5;
const neoPsychology = 2;
const neoChemistry = 3;

// indiana
const indianaAbsoluteOration = 78;
const indianaAbsoluteCrafts = 52;
const indianaAbsolutePsychology = 66;
const indianaAbsoluteChemistry = 80;


const indianaOration = 2;
const indianaCrafts = 0.5;
const indianaPsychology = 1;
const indianaChemistry = 2;
// severus
const severusAbsoluteOration = 75;
const severusAbsoluteCrafts = 67;
const severusAbsolutePsychology = 54;
const severusAbsoluteChemistry = 90;


const severusOration = 2;
const severusCrafts = 1;
const severusPsychology = 0.5;
const severusChemistry = 3;
// aladin
const aladinAbsoluteOration = 80;
const aladinAbsoluteCrafts = 52;
const aladinAbsolutePsychology = 68;
const aladinAbsoluteChemistry = 76;


const aladinOration = 2;
const aladinCrafts = 0.5;
const aladinPsychology = 1;
const aladinChemistry = 2;

//Credit

const orationCredit =  4;
const craftsCredit = 2;
const psychologyCredit = 7;
const chemistryCredit = 5;

const sumCredit = orationCredit + craftsCredit + psychologyCredit + chemistryCredit;


//Neo Statistics

const neoTotalPoints = neoAbsoluteChemistry + neoAbsoluteCrafts + neoAbsoluteOration + neoAbsolutePsychology;

const neoAveragePoints =  (neoAbsoluteChemistry + neoAbsoluteCrafts + neoAbsoluteOration + neoAbsolutePsychology)/4;

const neoAveragePercent = (neoAveragePoints * 100) / 100;

const neoGpa = (neoChemistry * chemistryCredit + neoCrafts * craftsCredit + neoOration * orationCredit + neoPsychology * psychologyCredit) / sumCredit;

//indiana Statistics

const indianaTotalPoints = indianaAbsoluteChemistry + indianaAbsoluteCrafts + indianaAbsoluteOration + indianaAbsolutePsychology;

const indianaAveragePoints =  (indianaAbsoluteChemistry + indianaAbsoluteCrafts + indianaAbsoluteOration + indianaAbsolutePsychology)/4;

const indianaAveragePercent = (indianaAveragePoints * 100) / 100;

const indianaGpa = (indianaChemistry * chemistryCredit + indianaCrafts * craftsCredit + indianaOration * orationCredit + indianaPsychology * psychologyCredit) / sumCredit;

//severus Statistics

const severusTotalPoints = severusAbsoluteChemistry + severusAbsoluteCrafts + severusAbsoluteOration + severusAbsolutePsychology;

const severusAveragePoints =  (severusAbsoluteChemistry + severusAbsoluteCrafts + severusAbsoluteOration + severusAbsolutePsychology)/4;

const severusAveragePercent = (severusAveragePoints * 100) / 100;

const severusGpa = (severusChemistry * chemistryCredit + severusCrafts * craftsCredit + severusOration * orationCredit + severusPsychology * psychologyCredit) / sumCredit;


//aladin

const aladinTotalPoints = aladinAbsoluteChemistry + aladinAbsoluteCrafts + aladinAbsoluteOration + aladinAbsolutePsychology;

const aladinAveragePoints =  (aladinAbsoluteChemistry + aladinAbsoluteCrafts + aladinAbsoluteOration + aladinAbsolutePsychology)/4;

const aladinAveragePercent = (aladinAveragePoints * 100) / 100;

const aladinGpa = (aladinChemistry * chemistryCredit + aladinCrafts * craftsCredit + aladinOration * orationCredit + aladinPsychology * psychologyCredit) / sumCredit;



//Output
//ABSOLUTE
//IF
if((neoTotalPoints > indianaTotalPoints) && (neoTotalPoints > severusTotalPoints) && (neoTotalPoints > aladinTotalPoints)){
    console.log("Best pointsum:The Chosen one")
} else if ((indianaTotalPoints > neoTotalPoints) && (indianaTotalPoints > severusTotalPoints) && (indianaTotalPoints > aladinTotalPoints)){
    console.log("Best pointsum:Indie")
} else if((severusTotalPoints > neoTotalPoints) && (severusTotalPoints > indianaTotalPoints) && (severusTotalPoints > aladinTotalPoints)){
    console.log("Best pointsum:SNAPE")
} else if((aladinTotalPoints > neoTotalPoints) && (aladinTotalPoints > indianaTotalPoints) && (aladinTotalPoints > severusTotalPoints)){
    console.log("Best pointsum:Prince Ali! Fabulous he! Ali Ababwa")
} else {
    console.log("ვაი")
}
//SWITCH
switch (true) {
    case ((neoTotalPoints > indianaTotalPoints) && (neoTotalPoints > severusTotalPoints) && (neoTotalPoints > aladinTotalPoints)):
        console.log("Best pointsum:The Chosen one");
        break;
    case ((indianaTotalPoints > neoTotalPoints) && (indianaTotalPoints > severusTotalPoints) && (indianaTotalPoints > aladinTotalPoints)):
        console.log("Best pointsum:Indie");
        break;
    case ((severusTotalPoints > neoTotalPoints) && (severusTotalPoints > indianaTotalPoints) && (severusTotalPoints > aladinTotalPoints)):
        console.log("Best pointsum:SNAPE");
        break;
    case ((aladinTotalPoints > neoTotalPoints) && (aladinTotalPoints > indianaTotalPoints) && (aladinTotalPoints > severusTotalPoints)):
        console.log("Best pointsum:Prince Ali! Fabulous he! Ali Ababwa");
        break;
    default: console.log("ვაი");
}
//Average

//IF
if((neoAveragePoints > indianaAveragePoints) && (neoAveragePoints > severusAveragePoints) && (neoAveragePoints > aladinAveragePoints)){
    console.log("Best Average:The Chosen one");
} else if ((indianaAveragePoints > neoAveragePoints) && (indianaAveragePoints > severusAveragePoints) && (indianaAveragePoints > aladinAveragePoints)){
    console.log("Best Average:Indie");
} else if((severusAveragePoints > neoAveragePoints) && (severusAveragePoints > indianaAveragePoints) && (severusAveragePoints > aladinAveragePoints)){
    console.log("Best Average:SNAPE");
} else if((aladinAveragePoints > neoAveragePoints) && (aladinAveragePoints > indianaAveragePoints) && (aladinAveragePoints > severusAveragePoints)){
    console.log("Best Average:Prince Ali! Fabulous he! Ali Ababwa");
} else {
    console.log("ვაი")
}

//SWITCH
switch (true) {
    case ((neoAveragePoints > indianaAveragePoints) && (neoAveragePoints > severusAveragePoints) && (neoAveragePoints > aladinAveragePoints)):
        console.log("Best average:The Chosen one");
        break;
    case ((indianaAveragePoints > neoAveragePoints) && (indianaAveragePoints > severusAveragePoints) && (indianaAveragePoints > aladinAveragePoints)):
        console.log("Best average:Indie");
        break;
    case ((severusAveragePoints > neoAveragePoints) && (severusAveragePoints > indianaAveragePoints) && (severusAveragePoints > aladinAveragePoints)):
        console.log("Best average:SNAPE");
        break;
    case ((aladinAveragePoints > neoAveragePoints) && (aladinAveragePoints > indianaAveragePoints) && (aladinAveragePoints > severusAveragePoints)):
        console.log("Best average:Prince Ali! Fabulous he! Ali Ababwa");
        break;
    default: console.log("ვაი");
}



//GPA

//IF
if((neoGpa > indianaGpa) && (neoGpa > severusGpa) && (neoGpa > aladinGpa)){
    console.log("Best GPA:The Chosen one")
} else if ((indianaGpa > neoGpa) && (indianaGpa > severusGpa) && (indianaGpa > aladinGpa)){
    console.log("Best GPA:Indie")
} else if((severusGpa > neoGpa) && (severusGpa > indianaGpa) && (severusGpa > aladinGpa)){
    console.log("Best GPA:SNAPE")
} else if((aladinGpa > neoGpa) && (aladinGpa > indianaGpa) && (aladinGpa > severusGpa)){
    console.log("Best GPA:Prince Ali! Fabulous he! Ali Ababwa")
} else {
    console.log("ვაი")
}

//SWITCH
switch (true) {
    case ((neoGpa > indianaGpa) && (neoGpa > severusGpa) && (neoGpa > aladinGpa)):
        console.log("Best GPA:The Chosen one")
        break;
    case ((indianaGpa > neoGpa) && (indianaGpa > severusGpa) && (indianaGpa > aladinGpa)):
        console.log("Best GPA:Indie")
        break;
    case ((severusGpa > neoGpa) && (severusGpa > indianaGpa) && (severusGpa > aladinGpa)):
        console.log("Best GPA:SNAPE")
        break;
    case ((aladinGpa > neoGpa) && (aladinGpa > indianaGpa) && (aladinGpa > severusGpa)):
        console.log("Best GPA:Prince Ali! Fabulous he! Ali Ababwa")
        break;
    default: console.log("ვაი");
}


// //Neo stat
// console.log("Neo");
// console.log(neoGpa);
// console.log(neoTotalPoints);
// console.log(neoAveragePoints);
// console.log(neoAveragePercent + "%");
//
// //indiana stat
// console.log("indiana");
// console.log(indianaGpa);
// console.log(indianaTotalPoints);
// console.log(indianaAveragePoints);
// console.log(indianaAveragePercent + "%");
//
// //severus stat
// console.log("Severus");
// console.log(severusGpa);
// console.log(severusTotalPoints);
// console.log(severusAveragePoints);
// console.log(severusAveragePercent + "%");
//
// //Aladin stat
// console.log("Aladin");
// console.log(aladinGpa);
// console.log(aladinTotalPoints);
// console.log(aladinAveragePoints);
// console.log(aladinAveragePercent + "%");