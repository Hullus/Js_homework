const A = false;
const B = true;
const C = false;
const F = true;
const G = 3;
const H = 10;
const I = -3;
const J = 10;
// D && E == undefined

console.log(A && (B || !D)); //(1) False

console.log((B && (!A || D)) != !F );  //(2) True

console.log((C || !(B && F)) && (A || E)); //(3) False

console.log((A && (B === F)) || (B || D) && !C); //(4) True

console.log(C || (B === !A) || ((E === D) === (!B === F))); //(5) True

console.log(!C && (J == H))//(6) True

console.log(A || !B || (H > I)); //(7) True

console.log(((B !== C) || (I > G)) != (!A && B)) //(8) False

console.log(!A || (J === C) && (E ||(C === !A))) //(9) True

console.log((A || (H > I)) && (J >= H) != (B || A )) //(10) False

